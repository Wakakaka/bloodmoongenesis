// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enemy.h"
#include "Components/CapsuleComponent.h"
#include "FireBall.h"
#include "Minion.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EnemyType : uint8
{
	Normal 	UMETA(DisplayName = "Normal"),
	Brute	UMETA(DisplayName = "Brute"),
	Head	UMETA(DisplayName = "Head")
};

UCLASS()
class PARTICLETEST_API AMinion : public AEnemy
{
	GENERATED_BODY()

private:

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	float atkResetDuration;
	float atkResetTimer = 0.0f;
	float stunTimer = 0.0f;
	float stunDuration = 1.0f;
	float distanceBetweenPoint = 200.0f;
	float followDuration = 6.0f;
	float followTimer = 0.0f;
	bool isLookAtPlayer = false;

public:	
	AMinion();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
	EnemyType enemyType;

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void SetLookAtPlayer(bool b);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
		UCapsuleComponent* attackCollider;

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void LookAtPlayer();

	bool inAtkRadius = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	bool isAtk = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	bool isFollowPlayer = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	float damage = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool isRoaming = false;

	FVector directionToPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Minion Behavior")
	float minRadius;

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	virtual void Kill();

	UFUNCTION(BlueprintImplementableEvent, Category = "Minion Behavior")
	void Attack();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Minion Behavior")
	void GetDamage(float d,FVector location);

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
	void ResetAnimation();

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void DoDamage();

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void ResetDamage();

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void StopFollowPlayer();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Minion Behavior")
	bool isStunned = false;

	UFUNCTION(BlueprintImplementableEvent, Category = "Minion Behavior")
	bool FindLineOfSight();

	UPROPERTY(EditAnywhere, Category = "Minion Behavior")
	TSubclassOf<AActor> ShootingPointBP;

	TArray<AActor*> shootingLine;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Minion Behavior")
	bool isShooting = false;

	void SpawnShootingPoint();
	void RepositionShootingPoint();
	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void DestroyShootingLine();
};
