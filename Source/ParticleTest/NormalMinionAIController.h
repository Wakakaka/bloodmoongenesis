// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MinionAIController.h"
#include "NormalMinion.h"
#include "NormalMinionAIController.generated.h"

UCLASS()
class PARTICLETEST_API ANormalMinionAIController : public AMinionAIController
{
	GENERATED_BODY()
	
private:
	class ANormalMinion* minionActor;

public:
	ANormalMinionAIController();
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	virtual void MoveToPlayer() override;
	virtual void Roaming() override;
};
