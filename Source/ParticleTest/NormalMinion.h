// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Minion.h"
#include "NormalMinion.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class MinionAttackType : uint8
{
	Claw UMETA(DisplayName = "Claw"),
	Lunge UMETA(DisplayName = "Lunge")
};

UCLASS()
class PARTICLETEST_API ANormalMinion : public AMinion
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

private:
	void SetAttackType();
	class ANormalMinionAIController* minionAI;

public:
	ANormalMinion();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
		MinionAttackType attackType;

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
		void Stun(FVector source);

	virtual void Kill() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	bool isOutOfBloodPool = false;
};
