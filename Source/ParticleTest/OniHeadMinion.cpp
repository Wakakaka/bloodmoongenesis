// Fill out your copyright notice in the Description page of Project Settings.


#include "OniHeadMinion.h"
#include "OniHeadMinionAIController.h"
#include "GameFramework/CharacterMovementComponent.h"

AOniHeadMinion::AOniHeadMinion()
{

}

void AOniHeadMinion::BeginPlay()
{
	Super::BeginPlay();
	minionAI = Cast<AOniHeadMinionAIController>(GetController());
	enemyType = EnemyType::Head;
}

void AOniHeadMinion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isStunned)
	{
		stunTimer += DeltaTime;
		if (stunTimer >= stunDuration)
		{
			stunTimer = 0.0f;
			isStunned = false;
		}
	}
}

void AOniHeadMinion::SetMaxSpeed(float speed)
{
	GetCharacterMovement()->MaxWalkSpeed = speed;
}

void AOniHeadMinion::Kill()
{
	Destroy();
}

void AOniHeadMinion::Stun(FVector source)
{
	stunTimer = 0.0f;
	if (!isStunned) isStunned = true;
	minionAI->StopMovement();
	FVector sourceLocation = source;
	sourceLocation.Z = GetActorLocation().Z;
	FVector directionToSource = sourceLocation - GetActorLocation();
	FRotator Rot = FRotationMatrix::MakeFromX(directionToSource).Rotator();
	SetActorRotation(Rot);
}


