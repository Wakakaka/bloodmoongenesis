#include "BruteMinion.h"
#include "BruteMinionAIController.h"

ABruteMinion::ABruteMinion()
{
	enemyType = EnemyType::Brute;
	maxHealth = 300.0f;
	currentHealth = maxHealth;
}

void ABruteMinion::BeginPlay()
{
	Super::BeginPlay();
	minionAI = Cast<ABruteMinionAIController>(GetController());
	SetMinionState();
	ResetStanceRecovery();
}

void ABruteMinion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isStunned)
	{
		stunTimer += DeltaTime;
		if (stunTimer >= stunDuration)
		{
			stunTimer = 0.0f;
			isStunned = false;
			ResetAnimation();
		}
	}

	if (bruteMinionState == BruteMinionState::Vulnerable)
	{
		vulnerableResetTimer += DeltaTime;
		if (vulnerableResetTimer >= vulnerableResetDuration)
		{
			vulnerableResetTimer = 0.0f;
			ResetVulnerable();
		}
	}

	if (!isAtk)
	{
		if (currentStance > 0.0f)
		{
			if (stanceRecoveryTimer >= stanceRecoveryDuration)
			{
				currentStance -= DeltaTime * stanceRecoverySpeed;
				if (currentStance <= 0.0f)
				{
					currentStance = 0.0f;
					stanceRecoveryTimer = 0.0f;
				}
			}
			else
			{
				stanceRecoveryTimer += DeltaTime;
			}
		}
	}
}

void ABruteMinion::Stun(FVector source)
{
	stunTimer = 0.0f;
	if (!isStunned) isStunned = true;
	minionAI->StopMovement();
	FVector sourceLocation = source;
	sourceLocation.Z = GetActorLocation().Z;
	FVector directionToSource = sourceLocation - GetActorLocation();
	FRotator Rot = FRotationMatrix::MakeFromX(directionToSource).Rotator();
	SetActorRotation(Rot);
}

void ABruteMinion::Kill()
{
	Destroy();
}

void ABruteMinion::AddStance(float s)
{
	if (bruteMinionState != BruteMinionState::Vulnerable)
	{
		currentStance += s;
		if (currentStance >= maxStance)
		{
			SetVulnerable();
			currentStance = 0.0f;
		}
		ResetStanceRecovery();
	}
}

void ABruteMinion::ResetStanceRecovery()
{
	stanceRecoveryTimer = 0.0f;
	stanceRecoveryDuration = (maxHealth / currentHealth) * 1.5f;
	stanceRecoverySpeed = (currentHealth / maxHealth) * 20.0f;
}

void ABruteMinion::SetMinionState()
{
	int randNum = FMath::RandRange(1, 100);
	if (randNum <= 40)
	{
		bruteMinionState = BruteMinionState::BlockWalk;
	}
	else
	{
		bruteMinionState = BruteMinionState::Walk;
	}
	SetWalkAnimation();
}

void ABruteMinion::SetVulnerable()
{	
	isAtk = false;
	bruteMinionState = BruteMinionState::Vulnerable;
	minionAI->StopMovement();
	PlayVulnerable();
}

void ABruteMinion::ResetVulnerable()
{
	SetMinionState();
	ResetAnimation();
}

void ABruteMinion::ResetAttack()
{
	isAtk = false;
	ResetAnimation();
}

void ABruteMinion::CheckSwing()
{
	int randNum = FMath::RandRange(1, 100);
	if (randNum <= 50)
	{
		bruteAttackType = BruteMinionAttackType::OneSwing;
	}
	else
	{
		bruteAttackType = BruteMinionAttackType::TwoSwing;
	}
}
