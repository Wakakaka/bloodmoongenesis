// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Minion.h"
#include "BruteMinion.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class BruteMinionAttackType : uint8
{
	
	OneSwing UMETA(DisplayName = "OneSwing"),
	TwoSwing UMETA(DisplayName = "TwoSwing"),
	ClubSlam UMETA(DisplayName = "ClubSlam")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class BruteMinionState : uint8
{
	Walk UMETA(DisplayName = "Walk"),
	BlockWalk UMETA(DisplayName = "BlockWalk"),
	Vulnerable UMETA(DisplayName = "Vulnerable"),
};

UCLASS()
class PARTICLETEST_API ABruteMinion : public AMinion
{
	GENERATED_BODY()
	
private:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	class ABruteMinionAIController* minionAI;
	float currentStance = 0.0f;
	float maxStance = 100.0f;
	float vulnerableResetTimer = 0.0f;
	float vulnerableResetDuration = 3.0f;
	void ResetStanceRecovery();
	float stanceRecoveryTimer = 0.0f;
	float stanceRecoveryDuration = 0.0f;
	float stanceRecoverySpeed = 0.0f;

public:
	ABruteMinion();

	BruteMinionAttackType bruteAttackType;

	UFUNCTION(BlueprintCallable, Category = "Minion Behavior")
	void Stun(FVector source);

	virtual void Kill() override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
		void PlaySwingAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
		void PlaySlamAttack();

	UFUNCTION(BlueprintImplementableEvent, Category = "Animation")
		void PlayVulnerable();

	UFUNCTION(BlueprintCallable, Category = "Stance")
	void SetVulnerable();

	UFUNCTION(BlueprintCallable, Category = "Stance")
	void ResetVulnerable();

	UFUNCTION(BlueprintCallable, Category = "Stance")
	void AddStance(float s);

	UFUNCTION(BlueprintCallable, Category = "Attack")
	void ResetAttack();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MinionState")
	BruteMinionState bruteMinionState;

	UFUNCTION(BlueprintImplementableEvent, Category = "MinionState")
	void SetWalkAnimation();

	UFUNCTION(BlueprintCallable, Category = "MinionState")
	void SetMinionState();

	void CheckSwing();
};
