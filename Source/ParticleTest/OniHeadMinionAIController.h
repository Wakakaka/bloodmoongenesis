// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MinionAIController.h"
#include "OniHeadMinionAIController.generated.h"

UCLASS()
class PARTICLETEST_API AOniHeadMinionAIController : public AMinionAIController
{
	GENERATED_BODY()
	
private:
	class AOniHeadMinion* minionActor;
	
	float explodeDuration = 5.0f;
	virtual void MoveToPlayer() override;

public:
	AOniHeadMinionAIController();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void Roaming() override;

	UPROPERTY(EditAnywhere, Category = "Minion Behavior")
	float explodeTimer = 0.0f;
};
