// Fill out your copyright notice in the Description page of Project Settings.


#include "OniHeadMinionAIController.h"
#include "OniHeadMinion.h"
#include "Engine/World.h"

AOniHeadMinionAIController::AOniHeadMinionAIController()
{

}

void AOniHeadMinionAIController::BeginPlay()
{
	Super::BeginPlay();
	playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	FindBossActor(GetWorld(), bossCharacterList);
	bossController = Cast<AMyAIController>(bossCharacterList[0]->GetController());
}

void AOniHeadMinionAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!minionActor)
	{
		minionPawn = GetPawn();
		minionActor = Cast<AOniHeadMinion>(minionPawn);
	}

	if (playerCharacter && minionActor)
	{
		distanceToPlayer = FVector::Distance(minionActor->GetActorLocation(), playerCharacter->GetActorLocation());
		minionActor->LookAtPlayer();
		if (!minionActor->isExploded || !minionActor->isStunned)
		{
			if(distanceToPlayer > 250.0f) MoveToPlayer();
			else if (distanceToPlayer <= 200.0f)
			{
				Roaming();
			}
			else if (distanceToPlayer > 200.0f && distanceToPlayer <= 250.0f)
			{
				StopMovement();
			}
		}
		if(!minionActor->isAtk)
		{			
			if (distanceToPlayer <= 250.0f)
			{
				minionActor->isAtk = true;
				minionActor->SetMaxSpeed(300.0f);
				minionActor->PlayExplode();
			}
		}
		else
		{
			if (!minionActor->isExploded)
			{				
				if (explodeTimer >= explodeDuration)
				{
					StopMovement();
					minionActor->isExploded = true;
					minionActor->Explode();
				}
				else
				{
					explodeTimer += DeltaTime;
				}
			}			
		}
	}
}

void AOniHeadMinionAIController::MoveToPlayer()
{
	if (!minionActor->inAtkRadius)
	{
		MoveToLocation(FVector(playerCharacter->GetActorLocation().X, playerCharacter->GetActorLocation().Y, 
			minionActor->GetActorLocation().Z), 100.0f, false);
	}
}

void AOniHeadMinionAIController::Roaming()
{
	MoveToLocation(minionActor->GetActorLocation() + (-minionActor->GetActorForwardVector()*150.0f), 100.0f, false);
}

