// Fill out your copyright notice in the Description page of Project Settings.


#include "BruteMinionAIController.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ABruteMinionAIController::ABruteMinionAIController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABruteMinionAIController::BeginPlay()
{
	Super::BeginPlay();
	playerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	FindBossActor(GetWorld(), bossCharacterList);
	bossController = Cast<AMyAIController>(bossCharacterList[0]->GetController());
}

void ABruteMinionAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!minionActor)
	{
		minionPawn = GetPawn();
		minionActor = Cast<ABruteMinion>(minionPawn);
	}

	if (minionActor->isStunned || !bossController->isEnterArena)
	{
		return;
	}

	if (minionActor->playerActor->CurrentHealth <= 0.0f || bossCharacterList[0]->currentHealth <= 0.0f)
	{
		StopMovement();
		return;
	}

	if (playerCharacter && minionActor && minionActor->bruteMinionState != BruteMinionState::Vulnerable)
	{
		distanceToPlayer = FVector::Distance(minionActor->GetActorLocation(), playerCharacter->GetActorLocation());
		if (distanceToPlayer <= 280.0f)
		{
			minionActor->inAtkRadius = true;
		}
		else
		{
			if (minionActor->inAtkRadius == true) minionActor->inAtkRadius = false;
		}

		if (!minionActor->isAtk)
		{
			MoveToPlayer();
		}
		else if (minionActor->inAtkRadius)
		{
			StopMovement();
			minionActor->isAtk = true;
			minionActor->CheckSwing();
			minionActor->PlaySwingAttack();
		}
	}
}

void ABruteMinionAIController::MoveToPlayer()
{
	if (!minionActor->inAtkRadius)
	{
		MoveToLocation(FVector(playerCharacter->GetActorLocation().X, playerCharacter->GetActorLocation().Y, minionActor->GetActorLocation().Z), 100.0f, false);
	}
}
